import Fraction as Fraction


fun main() {
    val zero = Point(0, 0)
    println(zero.toString())

    zero.shiftTo(1, 1)
    println(zero.toString())

    val one = Point(1, 1)
    println(zero.equals(one))

    val four = Any()

    println(zero.equals(four))
    print("პირველი წილადი: ")
    println(n1.toString())
    print("მეორე წილადი:")
    println(n2.toString())
    println("წილადების შეკრება")
    println(n1.toPlus())
    println("წილადების გამოკლება")
    println(n1.toMinus())
    println("წილადების გამრავლება")
    println(n1.toGam())
    println("წილადების გაყოფა")
    println(n1.toGak())
    println("წილადის გაყოფის შედეგად მიღებული წილადის შეკვეცა ")
    println(" წილადის მრიცხველიც და მნიშვნელიც იყოფა 5-ზე ")
    println("წილადის შეკვეცის შემდეგ მიიღება წილადი: ${n1.toShek()}")

}

class Point(private var x: Int, private var y: Int) {

    override fun toString(): String {
        return " x:$x და y:$y"

    }

    override fun equals(other: Any?): Boolean {
        if (other !is Point)
            return false
        else return (this.x == other.x) and (this.y == other.y)
    }

    fun shiftTo(x: Int, y: Int) {
        this.x = x
        this.y = y
    }

}

class Fraction(var n: Double, var d: Double) {
    override fun toString(): String {
        return "$n/$d"
    }
    fun toPlus(): String {
        return "${n1.n + n2.n}/${n1.d}"
    }
    fun toMinus(): String{
        return "${n1.n-n2.n}/${n1.d}"
    }
    fun toGam():String{
        return "${n1.n*n2.n}/${n1.d*n2.d}"
    }
    fun toGak():String{
        return "${n1.n*n2.d}/${n2.n*n1.d}"
    }
    fun  toShek():String{
        return "${n1.n*n2.d/5}/${n2.n*n1.d/5}"

    }
}
var n1 = Fraction(1.0, 5.0)
var n2 = Fraction(2.0, 5.0)